# Home
Estructura de datos y algoritmo con Python

## Material

El material está disponible en el siguiente [repositorio](https://gitlab.com/FAAM/python_eda), para obtener el código de fuente basta con que ejecutes el siguiente comando:

```
https://gitlab.com/FAAM/python_eda
```

## Contenidos

```{tableofcontents}
```